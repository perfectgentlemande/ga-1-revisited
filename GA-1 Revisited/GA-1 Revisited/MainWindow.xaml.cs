﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GA_1_Revisited
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string alphabetString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static readonly Random random = new Random(); //для генерации случайных чисел
        private static readonly object syncLock = new object(); //для генерации случайных чисел

        private int sizeOfGraph; //количество узлов
        private int[,] adjacencyMatrix = new int[1, 1]; //инициализации матрицы

        private int numberOfIndividuals; //количество особей в исходной популяции
        private int lengthOfChromosome; //длина хромосомы
        private char wayNode0; //путь от одного узла
        private char wayNode1; //путь к другому узлу
        private double probabilityOfMutation; //вероятность мутации
        private int numberOfCrossoverPoints; //количество точек скрещивания
        private int numberOfIterations; //количество итераций

        public MainWindow()
        {
            InitializeComponent();
            InitializeForm();

        }
        private void InitializeForm() //Инициализация формы
        {
            HideAllButtons();
            HideAllTextBoxes();
            HideAllTextBlocks();
        }

        //Взаимодействие
        private void buttonCreateTestGraph_Click(object sender, RoutedEventArgs e) //Создание графа
        {
            int result;
            if (int.TryParse(textBoxSizeOfGraph.Text, out result))
            {
                sizeOfGraph = result;
                adjacencyMatrix = new int[result, result];
                nullArray();
                buttonCreateTestGraphClickParameters();
                printArray();
            }
            else
            {
                MessageBox.Show("Вы ввели не число!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void buttonCreateConnection_Click(object sender, RoutedEventArgs e) //Создание связей
        {
            char node0;
            char node1;
            int weight;
            if (char.TryParse(textBoxNode0.Text, out node0) && char.TryParse(textBoxNode1.Text, out node1) && int.TryParse(textBoxWayWeight.Text, out weight))
            {
                if (checkIfExists(node0) && checkIfExists(node1))
                {
                    adjacencyMatrix[getNodeNumber(node0), getNodeNumber(node1)] = weight;
                    adjacencyMatrix[getNodeNumber(node1), getNodeNumber(node0)] = weight;
                    printArray();
                }
                else
                {
                    MessageBox.Show("Вы ввели узел которого нет!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            else
            {
                MessageBox.Show("Вы ввели не символ!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void buttonAcceptConnection_Click(object sender, RoutedEventArgs e) //Завершение редактирования
        {
            buttonAcceptConnectionClickParameters();
        }
        private void buttonCount_Click(object sender, RoutedEventArgs e) //Выполнение вычислений
        {
            double result;
            bool ConvertResult = double.TryParse(textBoxSizeOfGraph.Text, out result);
            if (int.TryParse(textBoxNumberOfIndividuals.Text, out numberOfIndividuals) && double.TryParse(textBoxProbabilityOfMutation.Text, out probabilityOfMutation) && int.TryParse(textBoxNumberOfCrossoverPoints.Text, out numberOfCrossoverPoints) && int.TryParse(textBoxNumberOfIterations.Text, out numberOfIterations) && int.TryParse(textBoxLengthOfChromosome.Text, out lengthOfChromosome) && char.TryParse(textBoxWayNode0.Text, out wayNode0) && char.TryParse(textBoxWayNode1.Text, out wayNode1))
            {
                buttonCountClickParameters();
                showCharacteristics();

                if (lengthOfChromosome < sizeOfGraph - 2)
                {
                    lengthOfChromosome = sizeOfGraph - 2;
                }
                if (numberOfCrossoverPoints < sizeOfGraph - 2)
                {
                    numberOfCrossoverPoints = sizeOfGraph - 2;
                }
                if (probabilityOfMutation < 0)
                {
                    probabilityOfMutation = 0;
                }
                if (probabilityOfMutation >= 1)
                {
                    probabilityOfMutation = 1.0;
                }
                count();
            }
            else
            {
                MessageBox.Show("Вы ввели не число!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void HideAllButtons() //Скрытие всех кнопок
        {
            this.buttonCreateConnection.Visibility = Visibility.Collapsed;
            this.buttonAcceptConnection.Visibility = Visibility.Collapsed;
            this.buttonCount.Visibility = Visibility.Collapsed;
        }
        private void HideAllTextBoxes() //Скрытие всех полей ввода
        {
            this.textBoxNode0.Visibility = Visibility.Collapsed;
            this.textBoxNode1.Visibility = Visibility.Collapsed;
            this.textBoxWayWeight.Visibility = Visibility.Collapsed;
            this.textBoxNumberOfIndividuals.Visibility = Visibility.Collapsed;
            this.textBoxWayNode0.Visibility = Visibility.Collapsed;
            this.textBoxWayNode1.Visibility = Visibility.Collapsed;
            this.textBoxProbabilityOfMutation.Visibility = Visibility.Collapsed;
            this.textBoxNumberOfCrossoverPoints.Visibility = Visibility.Collapsed;
            this.textBoxNumberOfIterations.Visibility = Visibility.Collapsed;
            this.textBoxLengthOfChromosome.Visibility = Visibility.Collapsed;
        }
        private void HideAllTextBlocks() //Скрытие текстовых блоков
        {
            this.textBlockNumberOfIndividuals.Visibility = Visibility.Collapsed;
            this.textBlockWayNode0.Visibility = Visibility.Collapsed;
            this.textBlockWayNode1.Visibility = Visibility.Collapsed;
            this.textBlockProbabilityOfMutation.Visibility = Visibility.Collapsed;
            this.textBlockNumberOfCrossoverPoints.Visibility = Visibility.Collapsed;
            this.textBlockNumberOfIterations.Visibility = Visibility.Collapsed;
            this.textBlockLengthOfChromosome.Visibility = Visibility.Collapsed;
        }
        private void buttonCreateTestGraphClickParameters() //Изменение параметров при нажатии кнопки buttonCreateTestGraph
        {
            buttonCreateTestGraph.Visibility = Visibility.Collapsed;
            textBoxSizeOfGraph.IsReadOnly = true;
            buttonCreateConnection.Visibility = Visibility.Visible;
            buttonAcceptConnection.Visibility = Visibility.Visible;
            textBoxNode0.Visibility = Visibility.Visible;
            textBoxNode1.Visibility = Visibility.Visible;
            textBoxWayWeight.Visibility = Visibility.Visible;
        }
        private void buttonAcceptConnectionClickParameters() //Изменение параметров при нажатии кнопки buttonCreateConnection
        {
            textBoxSizeOfGraph.Visibility = Visibility.Collapsed;
            textBoxNode0.Visibility = Visibility.Collapsed;
            textBoxNode1.Visibility = Visibility.Collapsed;
            textBoxWayWeight.Visibility = Visibility.Collapsed;
            buttonCreateConnection.Visibility = Visibility.Collapsed;
            buttonAcceptConnection.Visibility = Visibility.Collapsed;
            textBoxNode0.IsReadOnly = true;
            textBoxNode1.IsReadOnly = true;
            textBoxWayWeight.IsReadOnly = true;
            textBoxNumberOfIndividuals.Visibility = Visibility.Visible;
            textBoxWayNode0.Visibility = Visibility.Visible;
            textBoxWayNode1.Visibility = Visibility.Visible;
            textBoxProbabilityOfMutation.Visibility = Visibility.Visible;
            textBoxNumberOfCrossoverPoints.Visibility = Visibility.Visible;
            textBoxNumberOfIterations.Visibility = Visibility.Visible;
            textBoxLengthOfChromosome.Visibility = Visibility.Visible;
            textBlockNumberOfIndividuals.Visibility = Visibility.Visible;
            textBlockWayNode0.Visibility = Visibility.Visible;
            textBlockWayNode1.Visibility = Visibility.Visible;
            textBlockProbabilityOfMutation.Visibility = Visibility.Visible;
            textBlockNumberOfCrossoverPoints.Visibility = Visibility.Visible;
            textBlockNumberOfIterations.Visibility = Visibility.Visible;
            textBlockLengthOfChromosome.Visibility = Visibility.Visible;
            buttonCount.Visibility = Visibility.Visible;
        }
        private void buttonCountClickParameters() //Изменение параметров при нажатии кнопки buttonAcceptConnection
        {
            textBoxNumberOfIndividuals.IsReadOnly = true;
            textBoxWayNode0.IsReadOnly = true;
            textBoxWayNode1.IsReadOnly = true;
            textBoxProbabilityOfMutation.IsReadOnly = true;
            textBoxNumberOfCrossoverPoints.IsReadOnly = true;
            textBoxLengthOfChromosome.IsReadOnly = true;
        }
        private void showCharacteristics() //Вывод характеристик
        {
            textBlockListOfCharacteristics.Text += "Size of Graph: " + Convert.ToString(sizeOfGraph) + "\n";
            textBlockListOfCharacteristics.Text += "Number of Individuals: " + Convert.ToString(numberOfIndividuals) + "\n";
            textBlockListOfCharacteristics.Text += "Node 0 (Transmitter): " + Convert.ToString(wayNode0) + "\n";
            textBlockListOfCharacteristics.Text += "Node 1 (Receiver): " + Convert.ToString(wayNode1) + "\n";
            textBlockListOfCharacteristics.Text += "Probability of Mutation: " + Convert.ToString(probabilityOfMutation) + "\n";
            textBlockListOfCharacteristics.Text += "Number of Crossover Points: " + Convert.ToString(numberOfCrossoverPoints) + "\n";
            textBlockListOfCharacteristics.Text += "Number of Iterations: " + Convert.ToString(numberOfIterations) + "\n";
            textBlockListOfCharacteristics.Text += "Length of Chromosome: " + Convert.ToString(lengthOfChromosome);
        }
        private bool checkIfExists(char node) //Проверка наличия символа в графе
        {
            for (int i = 0; i < sizeOfGraph; i++)
            {
                if (alphabetString[i] == node)
                {
                    return true;
                }
            }
            return false;
        }
        private void nullArray() //Обнуление массива
        {
            for (int i = 0; i < sizeOfGraph; i++)
            {
                for (int j = 0; j < sizeOfGraph; j++)
                {
                    adjacencyMatrix[i, j] = 100500;
                    if (i == j)
                    {
                        adjacencyMatrix[i, j] = 0;
                    }
                }
            }
        }
        private void printArray() //Вывод массива
        {
            labelMatrix.Text = "";
            for (int i = 0; i < sizeOfGraph; i++)
            {
                for (int j = 0; j < sizeOfGraph; j++)
                {
                    labelMatrix.Text += Convert.ToString(adjacencyMatrix[i, j]);
                }
                labelMatrix.Text += "\n";
            }
        }
        private int getNodeNumber(char node) //Получение номера узла
        {
            for (int i = 0; i < sizeOfGraph; i++)
            {
                if (alphabetString[i] == node)
                {
                    return i;
                }
            }
            return 0;
        }

        //Логика
        private void count() //Сам алгоритм без дубликатов
        {
            //создание исходной популяции
            List<string> population = new List<string>();
            population = generatePopulation(Convert.ToString(wayNode0), Convert.ToString(wayNode1)).Distinct().ToList();

            //кусок алфавита, так как генератор случайных символов не выдает такой вариант решения
            /*string obviousSolution=""+wayNode0;
            for (int i=1; i<=lengthOfChromosome; i++)
            {
                obviousSolution += Convert.ToString(alphabetString[i]);
            }
            obviousSolution += wayNode1;
            population.Add(obviousSolution);
            */
            save(population, 1);

            string resultWayName = "";
            int resultWayCount = 0;

            Dictionary<string, int> wayWeight = new Dictionary<string, int>();
            Dictionary<string, int> sortedWays = new Dictionary<string, int>();
            Dictionary<string, int> newWayWeight = new Dictionary<string, int>();
            Dictionary<string, int> newSortedWays = new Dictionary<string, int>();

            //на случай сохранения хороших 
            Dictionary<string, int> acceptableWays = new Dictionary<string, int>();


            for (int iteration = 0; iteration < numberOfIterations && population.Count > 1; iteration++)
            {
                //подсчет расстояний и выбор хороших решений, если таковые есть
                for (int i = 0; i < population.Count; i++)
                {
                    wayWeight.Add(population[i], countWay(population[i]));
                    if (countWay(population[i]) < 100500 && !acceptableWays.ContainsKey(population[i]))
                    {
                        acceptableWays.Add(population[i], countWay(population[i]));
                    }
                }

                //тест
                save(wayWeight, 2);

                //сортировка
                var wayWeightToSort = wayWeight.OrderBy(x => x.Value);
                sortedWays = wayWeightToSort.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
                acceptableWays = acceptableWays.OrderBy(x => x.Value).ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);

                //тест
                save(sortedWays, 3);

                //скрещивание и выбор хороших решений, если таковые появились
                List<string> newPopulation = crossOver(sortedWays.Keys.ToList()).Distinct().ToList();
                for (int i = 0; i < newPopulation.Count; i++)
                {
                    if (countWay(newPopulation[i]) < 100500 && !acceptableWays.ContainsKey(newPopulation[i]))
                    {
                        acceptableWays.Add(newPopulation[i], countWay(newPopulation[i]));
                    }
                }

                //тест
                save(newPopulation, 4);

                //мутация
                if (probabilityOfMutation != 0)
                {
                    newPopulation = mutationControlled(newPopulation).Distinct().ToList();
                    //newPopulation = mutation(newPopulation).Distinct().ToList();
                }

                //тест
                save(newPopulation, 5);

                //пересчет сортированных путей и выбор хороших решений, если таковые появились
                for (int i = 0; i < newPopulation.Count; i++)
                {
                    newWayWeight.Add(newPopulation[i], countWay(newPopulation[i]));
                    if (countWay(newPopulation[i]) < 100500 && !acceptableWays.ContainsKey(newPopulation[i]))
                    {
                        acceptableWays.Add(newPopulation[i], countWay(newPopulation[i]));
                    }
                }

                //тест
                save(newWayWeight, 6);

                //сортировка
                wayWeightToSort = newWayWeight.OrderBy(x => x.Value);
                newSortedWays = wayWeightToSort.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
                acceptableWays = acceptableWays.OrderBy(x => x.Value).ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);

                //тест
                save(newSortedWays, 7);

                if (acceptableWays.Count != 0)
                {
                    resultWayName = acceptableWays.Keys.ElementAt(0);
                    resultWayCount = acceptableWays.Values.ElementAt(0);
                }
                else
                {
                    resultWayName = "";
                    resultWayCount = 100500;
                }
                population = newSortedWays.Keys.ToList();

                wayWeight.Clear();
                sortedWays.Clear();
                newWayWeight.Clear();
                newSortedWays.Clear();
            }

            if (resultWayCount == 100500)
            {
                textBlockResult.Text = "Результирующий путь не найден ";
            }
            else
            {
                acceptableWays = acceptableWays.OrderBy(x => x.Value).ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
                save(acceptableWays, 8);
                textBlockResult.Text = "Результирующий путь: " + Convert.ToString(resultWayName) + " длиной " + Convert.ToString(resultWayCount);
            }

        }
        private List<string> generatePopulation(string start, string end) //Создание популяции
        {
            List<string> populationList = new List<string>();
            string individual = "";
            for (int i = 0; i < numberOfIndividuals; i++)
            {
                individual += start;
                for (int j = 0; j < lengthOfChromosome; j++)
                {
                    individual += randomLetter();
                }
                individual += end;
                populationList.Add(individual);
                individual = "";
            }
            return populationList.Distinct().ToList();
        }
        private char randomLetter() //Выбор случайной буквы
        {
            int index = 0;
            lock (syncLock)
            {
                index = random.Next(0, sizeOfGraph);
            }
            return alphabetString[index];
        }
        private int countWay(string way) //Подсчет расстояния
        {
            int result = 0;
            for (int i = 0; i < way.Length - 1; i++)
            {
                result += adjacencyMatrix[getNodeNumber(way[i]), getNodeNumber(way[i + 1])];
            }
            return result;
        }
        private List<string> crossOver(List<string> oldPopulation) //Скрещивание
        {
            int[] arrayOfCrossoverPointIndexes = new int[numberOfCrossoverPoints];
            int index = 0;

            //выбор точек скрещивания случайным образом
            for (int i = 0; i < numberOfCrossoverPoints; i++)
            {
                lock (syncLock)
                {
                    index = random.Next(1, lengthOfChromosome + 1);
                }
                arrayOfCrossoverPointIndexes[i] = index;
                index = 0;
            }

            List<string> newPopulation = new List<string>();
            save(oldPopulation, 10);

            //сам процесс скрещивания
            for (int i = 0; i < oldPopulation.Count / 2; i++)
            {
                string firstChromosome = oldPopulation[i];
                string secondChromosome = oldPopulation[i + 1];

                for (int j = 0; j < arrayOfCrossoverPointIndexes.Length; j++)
                {
                    char letterFromFirstChromosome = firstChromosome[arrayOfCrossoverPointIndexes[j]];
                    char letterFromSecondChromosome = secondChromosome[arrayOfCrossoverPointIndexes[j]];

                    firstChromosome = firstChromosome.Remove(arrayOfCrossoverPointIndexes[j], 1).Insert(arrayOfCrossoverPointIndexes[j], letterFromSecondChromosome.ToString());
                    secondChromosome = secondChromosome.Remove(arrayOfCrossoverPointIndexes[j], 1).Insert(arrayOfCrossoverPointIndexes[j], letterFromFirstChromosome.ToString());
                }

                newPopulation.Add(firstChromosome);
                newPopulation.Add(secondChromosome);
            }
            return newPopulation.Distinct().ToList();
        }
        private List<string> mutationControlled(List<string> population) //Контролируема мутация 
        {
            //определение количества мутантов и массива с номерами мутирующих особей
            var numberOfMutants = Math.Round(population.Count * probabilityOfMutation);
            int[] indexesOfMutants = new int[Convert.ToInt32(numberOfMutants)];
            for (int i = 0; i < numberOfMutants; i++)
            {
                lock (syncLock)
                {
                    indexesOfMutants[i] = random.Next(0, population.Count - 1);
                }
            }

            for (int i = 0; i < indexesOfMutants.Length; i++)
            {
                //контролируемая мутация
                if (getFirstNumberOfMutationRange(population[i]) < lengthOfChromosome)
                {
                    int index = randomChromosomeIndex();
                    char newRandomLetter = randomLetter();
                    while ((population[indexesOfMutants[i]])[index] == newRandomLetter)
                    {
                        newRandomLetter = randomLetter();
                    }
                    population[indexesOfMutants[i]] = population[indexesOfMutants[i]].Remove(index, 1).Insert(index, Convert.ToString(newRandomLetter));
                }
                //неконтролируемая мутация
                else
                {
                    int index = randomChromosomeIndex();
                    char newRandomLetter = randomLetter();
                    while ((population[indexesOfMutants[i]])[index] == newRandomLetter)
                    {
                        newRandomLetter = randomLetter();
                    }
                    population[indexesOfMutants[i]] = population[indexesOfMutants[i]].Remove(index, 1).Insert(index, Convert.ToString(newRandomLetter));
                }
            }
            return population;
        }
        private int randomChromosomeIndex() //Получение случайного индекса
        {
            int index = 0;
            lock (syncLock)
            {
                index = random.Next(1, lengthOfChromosome);
            }
            return index;
        }
        private int getFirstNumberOfMutationRange(string way) //получение первого номера, с которого можно начать контролируемую мутацию
        {
            string tempWay = Convert.ToString(way[0]);
            for (int i = 1; i < way.Length; i++)
            {
                tempWay += Convert.ToString(way[i]);
                int tempCount = countWay(tempWay);
                if (tempCount >= 100500)
                {
                    return i - 1;
                }
            }
            return way.Length;
        }

        //Для отладки
        private void save(List<string> listToSave, int fileNo)
        {
            System.IO.File.WriteAllLines("WriteLines" + Convert.ToString(fileNo) + ".txt", listToSave.ToArray());
        }
        private void save(Dictionary<string, int> dictionaryToSave, int fileNo)
        {
            string[] arrayToSave = new string[dictionaryToSave.Count];
            int i = 0;
            foreach (var item in dictionaryToSave)
            {
                arrayToSave[i] = item.Key + " " + Convert.ToString(item.Value);
                i++;
            }
            System.IO.File.WriteAllLines("WriteLines" + Convert.ToString(fileNo) + ".txt", arrayToSave);
        }
    }
}
